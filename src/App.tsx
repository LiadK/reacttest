import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'rsuite/dist/rsuite.min.css'
import { Button } from 'rsuite';
import { useState,useEffect } from 'react';
import { type } from 'os';

function Counter(prop: any){

  const [count, setCount] = useState(0);
  useEffect(()=> {console.log(1)},[count])
  return  <div>
    <p>You clicked {count} times!!!</p>
    <Button style={{backgroundImage:"url('https://static01.nyt.com/images/2022/02/12/dining/JT-Chocolate-Chip-Cookies/JT-Chocolate-Chip-Cookies-master768.jpg')",backgroundSize:"cover", width:"400px", height:"400px"}} onClick={() => {setCount(count+1); console.log(typeof(prop))}}>
        
    </Button>
  </div>
}
function App() {
  return (
    <div className="App">
      <Counter>

      </Counter>
      
    </div>
  );
}
export default App;
